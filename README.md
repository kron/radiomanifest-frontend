# radiogramma-dynamic

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Crea nuovo repository da riga di comando
```
touch README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin http://10.23.0.7:10080/radiogrammapp/frontend.git
git push -u origin master
```
### Push un repo esistente dalla riga di comando
```
git remote add origin http://10.23.0.7:10080/radiogrammapp/frontend.git
git push -u origin master
```
